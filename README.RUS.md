MulTithreaded SCRAPER
===================

Здравствуйте, рад приветствовать Вас здесь. Это документация по библиотеке mt_scraper для python версии 3.

Описание
--------

Это проект многопоточного скрапера сайтов. Многопоточный режим работы ускоряет сбор данных с Вэба в несколько раз (более 10 на обычном стареньком рабочем ноутбуке). Для использования вам неободимо переопределить метод parse для своих нужд и пользоваться благами многопоточности (со всей ее кривенькой реальзацией на Python)

Сбор данных итдет в JSON файл, в котором храняться списко объектов (словарей) с собранными данными.

Применение
----------

### Простое применение

#### Главный сценарий испоользования библиотеки

```
import mt_scraper

scraper = mt_scraper.Scraper()
scraper.run()
```

Как видите в нем всего три строчки кода

#### Что при этом происходит

При таком применении вы получаете скрапер данных со страниц из списка:
```
url_components_list = [
    'http://example.com/',
    'http://scraper.iamengineer.ru',
    'http://scraper.iamengineer.ru/bad-file.php',
    'http://badlink-for-scarper.ru',
]
```
Две последние страницы добавлены для демонстрации двух самых распространенных ошибок при получении данных из интернета, это HTTP 404 - Not Found, и URL Error: Name or service not known.

Настоящий URL получается при подстановке этого списка в шаблон:
```
url_template = '{}'
```
Данные накапливаются в файле:
```
out_filename = 'out.json'
```
Работа ведется в 5 потоков и создается очередь задачь количеством 5 эдиниц (это имееет значение например при отмене операции с клавиатуры, длинна очереди говорит о том сколько задачь отправлено на выполнение):
```
threads_num = 5
queue_len = 5
```
В качестве функции парсера используется вот такая:
```
def parse(self, num, url_component, html):
    '''Необходимо переопределить этот метод
    Должен возвращять словарь или None, если парсинг странички
    невозможен 
    '''
    parser = MyDummyHTMLParser()
    parser.feed(html)
    obj = parser.obj
    obj['url_component'] = url_component
    return parser.obj
```
DummyParser - это простая верисия HTML-парсера, она примечательна лишь тем, что использует только одну стандартную библиотеку и не требует никаких дополнительных модулей
Файл dummy_parser.py:
```
from html.parser import HTMLParser

class MyDummyHTMLParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.a_tag = False
        self.h1_tag = False
        self.p_tag = False
        self.obj = {}

    def handle_starttag(self, tag, attrs):
        if tag == 'h1':
            self.h1_tag = True
        elif tag == 'p':
            self.p_tag = True
        elif tag == 'a':
            self.a_tag = True
            for (attr, value, ) in attrs:
                if attr == 'href':
                    self.obj['link'] = value

    def handle_endtag(self, tag):
        if tag == 'h1':
            self.h1_tag = False
        elif tag == 'p':
            self.p_tag = False
        elif tag == 'a':
            self.a_tag = False

    def handle_data(self, data):
        if self.h1_tag:
            self.obj['header'] = data
        elif self.p_tag and not self.a_tag:
            self.obj['article'] = data
```
Такой подход используется только для демонстрации возможностей многопоточности, в реальных проектах рекомендуется использовать библиотеки lxml или BS, более продвинутое применение будет показано ниже в разделе "Продвинутое применение"

### Описание параметров коммандной строки

### Продвинутое применение