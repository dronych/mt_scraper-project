import os
from unittest import TestCase
from json import load

from mt_scraper import Scraper

class FullTestCase(TestCase):
    filename = 'testout.json'

    def setUp(self):
        if os.path.exists(self.filename):
            os.remove(self.filename)

    def tearDown(self):
        if os.path.exists(self.filename):
            os.remove(self.filename)

    def is_example_com_in_list(self, obj_list):
        example_com_in_list = False
        for o in obj_list:
            if o['url_component'] == 'http://example.com/':
                self.assertEqual(
                    o['header'],
                    "Example Domain"
                )
                self.assertEqual(
                    o['link'],
                    "http://www.iana.org/domains/example"
                )
                self.assertEqual(
                    o['article'],
                    "This domain is established to be used for illustrative examples in "\
                    "documents. You may use this\n    domain in examples without prior "\
                    "coordination or asking for permission."
                )
                example_com_in_list = True 

        self.assertEqual(example_com_in_list, True)

    def test_Scraper_run(self):

        scraper = Scraper()
        scraper.run(['-f', self.filename, '-m'])

        with open(self.filename) as infile:
            obj_list = load(infile)


        self.assertEqual(
            len(obj_list),
            2
        )
        self.is_example_com_in_list(obj_list)

    def test_Scraper_run_with_html_file(self):
        self.filename = 'testout.json'

        scraper = Scraper()
        scraper.run(['-f', self.filename, '-m', '--html-file', 'example.com.html'])

        with open(self.filename) as infile:
            obj_list = load(infile)


        self.assertEqual(
            len(obj_list),
            4
        )
        self.is_example_com_in_list(obj_list)

    def test_Scraper_run_bad_file_in_cmd(self):

        scraper = Scraper()
        with self.assertRaises(SystemExit) as cm:
            scraper.run('-u filename.fake'.split())

    # the_exception = cm.exception
    # self.assertEqual(the_exception.error_code, 3)


class CmdTestCase(TestCase):
    def test_list_in_cmd(self):
        scraper = Scraper()
        scraper._Scraper__parse_args('-l 100 200 300'.split())
        self.assertEqual(
                    scraper._Scraper__get_url_component_list(),
                    ['100', '200', '300', ]
                )

    def test_bad_file_in_cmd(self):
        scraper = Scraper()
        scraper._Scraper__parse_args('-u filename.fake'.split())
        self.assertRaises(
                    #SystemExit,
                    FileNotFoundError,
                    scraper._Scraper__get_url_component_list
                )
